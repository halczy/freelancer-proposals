10.times do
  Proposal.create!(
      customer: Faker::Company.name + " " + Faker::Company.suffix,
      portfolio_url: Faker::Internet.url,
      tools: Faker::App.name,
      estimated_hours: Faker::Number.number(2),
      hourly_rate: Faker::Number.number(2),
      weeks_to_complete: Faker::Number.number(2),
      client_email: Faker::Internet.email
  )
end